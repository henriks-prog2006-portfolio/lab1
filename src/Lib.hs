module Lib
    ( addAge, addNumber, mhead1,
     mhead2, mhead3, mhead4,
     mhead5, mhead6
    ) where

-- | Adds two integers together.
--
-- >>> addAge 5 7
-- 12
--
-- >>> addAge (-5) 7
-- 2
--
-- >>> addAge 0 0
-- 0
--age function

newtype Age = Age Int
addMyAge :: Age -> Age -> Age
addMyAge (Age x) (Age y) = Age (x + y)

addAge :: Int -> Int -> Int
addAge a b = a + b

-- | Adds two numbers together.
--
-- >>> addNumber 5 7
-- 12
--
-- >>> addNumber 3.5 2.5
-- 6.0
--
-- >>> addNumber (1 :: Integer) (2 :: Integer)
-- 3
--number function
addNumber :: Num a => a -> a -> a
addNumber x y = x + y


--adding the tasks required signature at the start of each function
--using "xs !! 0" instead of head to grab the first element, could also use "mhead1 xs" to destructure and grab it
-- 1. Using function argument pattern matching:
mhead1 :: [a] -> a
mhead1 (x:_) = x
mhead1 _     = error "Empty list"

-- |
-- >>> mhead1 [1, 2, 3]
-- 1
-- >>> mhead1 ["apple", "banana", "orange"]
-- "apple"
-- >>> mhead1 ([] :: [Int])
-- *** Exception: Empty list

-- 2. Using function guards:
mhead2 :: [a] -> a
mhead2 xs
    | not (null xs) = xs !! 0
    | otherwise     = error "Empty list"

-- |
-- >>> mhead2 [1, 2, 3]
-- 1
-- >>> mhead2 ["apple", "banana", "orange"]
-- "apple"
-- >>> mhead2 ([] :: [Int])
-- *** Exception: Empty list


-- 3. Using if ... else ... expressions:
mhead3 :: [a] -> a
mhead3 xs = if not (null xs) then xs !! 0 else error "Empty list"

-- |
-- >>> mhead3 [1, 2, 3]
-- 1
-- >>> mhead3 ["apple", "banana", "orange"]
-- "apple"
-- >>> mhead3 ([] :: [Int])
-- *** Exception: Empty list


-- 4. Using let .. in ..:
mhead4 :: [a] -> a
mhead4 xs =
    let h = xs !! 0
    in if not (null xs) then h else error "Empty list"

-- |
-- >>> mhead4 [1, 2, 3]
-- 1
-- >>> mhead4 ["apple", "banana", "orange"]
-- "apple"
-- >>> mhead4 ([] :: [Int])
-- *** Exception: Empty list


-- 5. Using where expression:
mhead5 :: [a] -> a
mhead5 xs = h
    where
        h = if not (null xs) then xs !! 0 else error "Empty list"

-- |
-- >>> mhead5 [1, 2, 3]
-- 1
-- >>> mhead5 ["apple", "banana", "orange"]
-- "apple"
-- >>> mhead5 ([] :: [Int])
-- *** Exception: Empty list


-- 6. Using case .. of .. expression:
mhead6 :: [a] -> a
mhead6 xs = case xs of
    (x:_) -> x
    _     -> error "Empty list"


-- |
-- >>> mhead6 [1, 2, 3]
-- 1
-- >>> mhead6 ["apple", "banana", "orange"]
-- "apple"
-- >>> mhead6 ([] :: [Int])
-- *** Exception: Empty list