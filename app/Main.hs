module Main (main) where

import Lib

main :: IO ()
main = do
    putStrLn "Hello, World"
    putStrLn "What is your name?"
    name <- getLine

    putStrLn "How old are you?"
    ageStr <- getLine
    let age = read ageStr :: Int

    let futureAge = age + 10

    putStrLn $ "Hello " ++ name ++ ", in 10 years you will be " ++ show futureAge ++ "."


    --testing addage and addnumber
    putStrLn "addage and addnumber test:"
    let result1 = addNumber 5 7
    let result2 = addNumber 3.5 2.5
    let result3 = addAge  25 30
    print result1
    print result2
    print result3
    putStrLn "addAge is better for enforcing a strict constraint and not allowing misuse of the function"
    putStrLn "addNumber is better for flexibility and allows a broader range of types"


    let x = [1..10]
    putStrLn "Our list is:"
    print (x)
    putStrLn "The first element of the list is:"
    print (head x)
    putStrLn "The first element of the mhead1 list is:"
    print (mhead1 x)
    putStrLn "The first element of the mhead2 list is:"
    print (mhead2 x)
    putStrLn "The first element of the mhead3 list is:"
    print (mhead3 x)
    putStrLn "The first element of the mhead4 list is:"
    print (mhead4 x)
    putStrLn "The first element of the mhead5 list is:"
    print (mhead5 x)
    putStrLn "The first element of the mhead6 list is:"
    print (mhead6 x)