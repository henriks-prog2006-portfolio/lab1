# Lab1 - Hello World (Haskell)

Introducing the basics of haskell

## Learning objectives for Lab1, as outlined by lecturer

> ### Objectives for Lab1
> - Learning basic syntax for haskell
> - Learning basic structure of haskell projects
> - Learning basic tools for haskell


----------------


# The tasks

### Task 1: Hello World
```
Write the simplest program in Haskell that prints "Hello World" 
to the standard output.
```
#### Output:
```
Hello World
```
--------------
### Task 2: Hello Name
```
Modify your program, by asking user for name first with a prompt: "What is your name?"
and then printing text: "Hello " concatenated with the name of the person.
```
#### Input:
```
Henrik
```
#### Output:
```
Hello Henrik
```
--------------
### Task 3: Age
```
Modify your program, by asking the user for their Name and their age,
and print the following repsonse:
Hello (name), in 10 years you will be (calculated_age).
```
#### Input:
```
Henrik
55
```
#### Output:
```
Hello Henrik, in 10 years you will be 65.
```
--------------
### Task 4: Type Safety
> - Create two functions, called addAge, and addNumber.
> - addNumber should be able to take in two numbers, and add them together. It should be a generic function that works on any numerical type. What is it that you need to use, and how?
> - addAge however, should only take Age instances as parameters value, and return a new Age instance that is a sum of the two Age instances.  Age should be backed by Int.
> - Version A: If you try to pass a generic number to addAge as a parameter, the compiler should prevent you from doing it.
> - Version B: If you call the function with Int it will still work, even though your function will have a signature
#### Input:
```
5 7
3.5 2.5
25 30
```
#### Output:
```
12
6.0
55
```
#### Explain the difference between version A and B:
```
addAge is better for enforcing a strict constraint and not allowing misuse of the function.
addNumber is better for flexibility and allows a broader range of types.
```
--------------
### Task 5: mhead
```
Haskell has a built-in functions to operate on lists. As exercises we will re-define them as our own functions, and prefix them with m.
head is a standard function that returns the first element of a given list.
Your task is to write several variants of your own function that returns the first element of the list, without using the built-in funtion head
The more different ways you can implement it, the better. Try to find as many different ways as you can. You can and should use ChatGPT for finding the different ways.
Make sure that you understand how your different implementations work.
Type your code yourself to memorize the patternns better.
```
#### Input:
```
[1..10]
```
#### Output:
Using head:
```
1
```
Using function argument pattern matching:
```
1
```
Using function guards:
```
1
```
Using if else:
```
1
```
Using let in:
```
1
```
Using where:
```
1
```
Using case of:
```
1
```